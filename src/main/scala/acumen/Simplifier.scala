package acumen

import Errors._
import Pretty._
import GE._
import util.Names._

// Simplifier: remove Intervals and replace them with the mid point.

object Simplifier extends util.ASTMap {
  override def mapExpr(e: Expr): Expr = super.mapExpr(e match {
    case ExprInterval(lo, hi) => Op(Name("/", 0), List(Op(Name("+", 0), List(lo, hi)), Lit(GDouble(2.0))))
    case ExprIntervalM(mid, _) => mid
    case expr => expr
  })
  def run(t: Prog): Prog = mapProg(t)
  def simplify(p: Prog): Prog = {
    Prog(p.defs map (simplify(p, _)))
  }

  def simplify(p: Prog, c: ClassDef): ClassDef =
    c match {
      case ClassDef(cn, fs, is, b) =>
        ClassDef(cn, fs, is map { init: Init => simplify(init) }, simplify(b))
    }

  def simplify(init: Init): Init = {
    init match {
      case Init(x, rhs) =>
        val drhs = rhs match {
          case NewRhs(cn, es) => NewRhs(cn, es map (simplify(_)))
          case ExprRhs(e) => ExprRhs(simplify(e))
        }
        Init(x, drhs)
    }
  }

  def simplify(as: List[Action]): List[Action] =
    as map { a: Action => simplify(a) }

  def simplify(a: Action): Action = {
    val dese = simplify(_: Expr)
    val desa = simplify(_: List[Action])
    val desc = simplify(_: Clause)
    val desca = simplify(_: ContinuousAction)
    val desda = simplify(_: DiscreteAction)
    a match {
      case IfThenElse(c, t, e) => IfThenElse(dese(c), desa(t), desa(e))
      case Switch(s, cls) => Switch(dese(s), cls map desc)
      case ForEach(x, e, b) => ForEach(x, dese(e), (simplify(b)))
      case Continuously(ca) => Continuously(desca(ca))
      case Discretely(da) => Discretely(desda(da))
    }
  }

  def simplify(e: Expr): Expr = {
    val des = simplify(_: Expr)
    e match {
      case Lit(gv) => e
      case Var(x) => e
      case Op(f, es) => Op(f, es map des)
      case Index(f, i) => Index(des(f), i map (des(_)))
      case Dot(o, f) => Dot(des(o), f)
      case ExprVector(es) => ExprVector(es map des)
      case Sum(e, i, col, cond) =>
        Sum(simplify(e), i, des(col), simplify(cond))
      case ExprLet(bs, e2) => ExprLet(bs map (b => (b._1, simplify(b._2))),
        simplify(e2))
      case TypeOf(cn) => e
      case ExprInterval(lo, hi) => Op(Name("/", 0), List(Op(Name("+", 0), List(lo, hi)), Lit(GDouble(2.0))))
      case ExprIntervalM(mid, _) => mid
    }
  }

  def simplify(e: ContinuousAction): ContinuousAction = {
    val des = simplify(_: Expr)
    e match {
      case EquationI(lhs, rhs) => EquationI(des(lhs), des(rhs))
      case EquationT(lhs, rhs) => EquationT(des(lhs), des(rhs))
      case _ => throw new ShouldNeverHappen
    }
  }

  def simplify(e: DiscreteAction): DiscreteAction = {
    val des = simplify(_: Expr)
    e match {
      case Assign(lhs, rhs) => Assign(des(lhs), des(rhs))
      case Create(lhs, cn, args) => Create(lhs map des, cn, args map des)
      case Elim(e) => Elim(des(e))
      case Move(o, p) => Move(des(o), des(p))
    }
  }

  def simplify(e: Clause): Clause =
    e match {
      case Clause(lhs, inv, rhs) => Clause(lhs, inv, simplify(rhs))
    }

  // Flatten nesting plus or times
  def flattenExpr(op: String, e: Expr): List[Expr] = op match {
    case "+" => e match {
      case Op(Name("+", 0), es) =>
        es.foldLeft(List[Expr]())((r, x) => r ::: flattenExpr("+", x))
      case _ => List(e)
    }
    case "*" => e match {
      case Op(Name("*", 0), es) =>
        es.foldLeft(List[Expr]())((r, x) => r ::: flattenExpr("*", x))
      case _ => List(e)

    }
  }

  // Aggregate list of numbers with plus or times
  def aggregateNums(op: String, l1: List[Lit], l2: List[Lit]): Lit = {
    val init = op match {
      case "+" => Lit(GDouble(0.0))
      case "*" => Lit(GDouble(1.0))
    }
    (l1 ::: l2).foldLeft(init)((r, x) => r match {
      case Lit(GDouble(n1)) => x match {
        case Lit(GInt(n2)) => op match {
          case "+" => Lit(GDouble(n1 + n2.toDouble))
          case "*" => Lit(GDouble(n1 * n2.toDouble))
        }
        case Lit(GDouble(n2)) => op match {
          case "+" => Lit(GDouble(n1 + n2))
          case "*" => Lit(GDouble(n1 * n2))
        }
      }
    })
  }

  def plusTimesPlus(e1: Op, e2: Op): Op = e1 match {
    case Op(Name("+", 0), es1) => e2 match {
      case Op(Name("+", 0), es2) =>
        Op(Name("+", 0), es1.map(x =>
          Op(Name("+", 0), es2.map(y => mkOp("*", x, y)))))
    }
  }
 
  def ruleWrapper(rule: (Expr, Expr) => Option[Expr], x: Expr, y: Expr): Option[Expr] = {
    rule(x, y) match {
      case Some(n) => Some(n)
      case None => rule(y, x) // Try other side
    }
  }
  // x * x => x^2
  def ruleTimesToExp(x: Expr, y: Expr) = {
    if (x == y)
      Some(mkOp("^", x, Lit(GInt(2))))
    else
      None
  }
  def ruleSinCosSqure(x: Expr, y: Expr) = {
    val one = Lit(GDouble(1.0))
    (x, y) match {
      case (Op(Name("*", 0), Op(Name("sin", 0), t1) :: Op(Name("sin", 0), t2) :: Nil),
        Op(Name("*", 0), Op(Name("cos", 0), t3) :: Op(Name("cos", 0), t4) :: Nil)) =>
        if (t1 == t2 && t3 == t4 && t1 == t3)
          Some(one)
        else
          None
      case (Op(Name("*", 0), Op(Name("sin", 0), t1) :: Op(Name("sin", 0), t2) :: Nil),
        Op(Name("^", 0), Op(Name("cos", 0), t3) :: Lit(GInt(2)) :: Nil)) =>
        if (t1 == t2 && t2 == t3)
          Some(one)
        else
          None
      case (Op(Name("*", 0), Op(Name("cos", 0), t1) :: Op(Name("cos", 0), t2) :: Nil),
        Op(Name("^", 0), Op(Name("sin", 0), t3) :: Lit(GInt(2)) :: Nil)) =>
        if (t1 == t2 && t2 == t3)
          Some(one)
        else
          None
      case (Op(Name("^", 0), Op(Name("sin", 0), t1) :: Lit(GInt(2)) :: Nil),
        Op(Name("^", 0), Op(Name("cos", 0), t2) :: Lit(GInt(2)) :: Nil)) =>
        if (t1 == t2)
          Some(one)
        else
          None
      case _ => None
    }
  }
  def combineTermsMultipleRules(es: List[Expr], rule1: (Expr, Expr) => Option[Expr],
    rule2: (Expr, Expr) => Option[Expr]): List[Expr] = {
    combineTerms(combineTerms(es)(rule1))(rule2)

  }
  def combineTerms(es1: List[Expr])(rule: (Expr, Expr) => Option[Expr]): List[Expr] = {
    var es = es1
    var result = es
    for (x <- es)
      for (y <- es.drop(es.indexOf(x) + 1)) {
        ruleWrapper(rule, x, y) match {
          case Some(e) =>
            result = e :: result;
            result = result.diff(List(x, y));
            es = es.diff(List(x, y));
          case None =>
        }
      }
    result
  }

  def simplifyEquation(e: Equation): Equation = {
    val result = Equation(simplifyE(e.lhs), simplifyE(e.rhs))
    result
   
  }

  def simplifyE(e: Expr): Expr = {
    unFlattenOp(simplifyExpr(e))
  }
  def simplifyExpr(e: Expr): Expr = e match {
    case Lit(_) => e
    case Var(_) => e
    case Op(Name("-", 0), a :: b :: Nil) => simplifyExpr(mkOp("+", simplifyExpr(a), mkOp("*", Lit(GDouble(-1.0)), b)))
    case Op(Name("/", 0), a :: b :: Nil) => {
      val sa = simplifyExpr(a)
      val sb = simplifyExpr(b)
      if (sa == Lit(GDouble(0.0)))
        Lit(GDouble(0.0))
      else {
        (tearProduct(sa), tearProduct(sb)) match {
          case ((na, Some(ra)), (nb, Some(rb))) => (na / nb) match {
            case 1.0 => mkOp("/", ra, rb)
            case n => mkOp("/", simplifyExpr(mkOp("*", Lit(GDouble(n)), ra)), rb)
          }
          case ((na, Some(ra)), (nb, None)) => (na / nb) match {
            case 1.0 => ra
            case n => simplifyExpr(mkOp("*", Lit(GDouble(n)), ra))
          }
          case ((na, None), (nb, Some(rb))) => mkOp("/", Lit(GDouble(na / nb)), rb)

          case ((na, None), (nb, None)) => Lit(GDouble(na / nb))
        }
      }
    }
    case Op(Name("+", 0), es) => {
      val ses = es.map(simplifyE(_))
      // No speration between varis and division
      val sorted1 = sortExprs(ses, false)
      // Flatten nesting pluses 
      val flattenSums = sorted1._3.map(x => flattenExpr("+", x)).flatten
      val sorted2 = sortExprs(flattenSums, false)
      var varis = sorted1._2 ::: sorted2._2
      val products = (sorted1._4 ::: sorted2._4).map(simplifyExpr)
      val numsInProducts = products.filter(x => x match { case Lit(GDouble(n)) => true; case _ => false })
      var varsInProducts = products.diff(numsInProducts) ::: varis
      varsInProducts = combineTerms(varsInProducts)(ruleSinCosSqure)
      val num = aggregateNums("+", sorted1._1, sorted2._1 ::: numsInProducts.asInstanceOf[List[Lit]])
      varis = varsInProducts
      val result: Expr = (num, varis) match {
        case (Lit(GDouble(0.0)), Nil) => Lit(GDouble(0.0))
        case (Lit(GDouble(0.0)), v :: Nil) => v
        case (Lit(GDouble(0.0)), l) => Op(Name("+", 0), l)
        case (n, Nil) => n
        case (n, l) => Op(Name("+", 0), n :: l)
      }
      result
    }
    case Op(Name("*", 0), es) => {
      var result: Expr = Lit(GDouble(1))
      val ses = es.map(simplifyE)
      // Seperate varis with divisions
      val sorted = sortExprs(ses, true)
      val divis = sorted._5
      val aggreProducts: List[Expr] = sorted._4.foldLeft(List[Expr]())((r, x) => x.es ::: r)
      val numsInProducts = aggreProducts.filter(x => x match { case Lit(GDouble(_)) | Lit(GInt(_)) => true; case _ => false })
      val varsInProducts = aggreProducts.diff(numsInProducts)
      val num: Expr = aggregateNums("*", numsInProducts.asInstanceOf[List[Lit]], sorted._1)
      var varis = sorted._2 ::: varsInProducts
      varis = combineTerms(varis)(ruleTimesToExp)
      val const: Expr = (num, varis) match {
        case (Lit(GDouble(0.0)), _) => Lit(GDouble(0.0))
        case (Lit(GDouble(1)), Nil) => Lit(GDouble(1))
        case (Lit(GDouble(1)), l) =>
          if (l.length > 1)
            Op(Name("*", 0), l)
          else
            l(0)

        case (n, Nil) => n
        case (n, l) => Op(Name("*", 0), n :: l)
      }

      val sums = sorted._3
      if (sums.length > 0) {
        var sumsProduct: Expr = Lit(GInt(1))
        if (sums.length > 0)
          sumsProduct = simplifyExpr(Op(Name("+", 0), flattenExpr("+", sums.drop(1).foldLeft(sums(0))((r, x) => plusTimesPlus(r, x)))))
        result = sumsProduct match {
          case Op(Name("+", 0), es) => const match {
            case Lit(GDouble(1.0)) => sumsProduct
            case n => Op(Name("+", 0), es.map(x => simplifyExpr(mkOp("*", n, x))))
          }
          case _ => mkOp("*", const, sumsProduct)
        }

      } else {
        result = const
      }
      // Combie the previous result with divisions 
      if (divis.length > 0) {
        val numerators = divis.map(x => x match { case Op(Name("/", 0), a :: b :: Nil) => a })
        val denumerators = divis.map(x => x match { case Op(Name("/", 0), a :: b :: Nil) => b })
        val fnumerator = simplifyExpr(Op(Name("*", 0), result :: numerators))
        val fdenumerator = divis.length match {
          case 1 => denumerators(0)
          case _ => Op(Name("*", 0), denumerators)
        }
        mkOp("/", fnumerator, fdenumerator)
      } else
        result
    }
    case Op(Name("^", 0), x::n::Nil)  => (simplifyE(x),simplifyE(n)) match{
      case ((Lit(GDouble(a)), Lit(GInt(b)))) => Lit(GDouble(Math.pow(a, b)))
      case ((Lit(GInt(a)), Lit(GInt(b)))) => Lit(GDouble(Math.pow(a, b)))
       case ((Lit(GInt(a)), Lit(GDouble(b)))) => Lit(GDouble(Math.pow(a, b)))
      case ((Lit(GDouble(a)), Lit(GDouble(b)))) => Lit(GDouble(Math.pow(a, b)))
      case _ => e
    }
    case ExprVector(es) => ExprVector(es map simplifyE)
    case e => e
  }

  // Return (nums, varis, sums, products,division)
  def sortExprs(es: List[Expr], division: Boolean): (List[Lit], List[Expr], List[Op], List[Op], List[Op]) = {
    var nums = List[Lit]()
    var vars = List[Expr]()
    var sums = List[Op]()
    var pros = List[Op]()
    var divis = List[Op]()
    es.map(x => x match {
      case Lit(GInt(n)) => nums = Lit(GDouble(n.toDouble)) :: nums
      case Lit(GDouble(n)) => nums = Lit(GDouble(n)) :: nums
      case Var(_) => vars = x :: vars
      case Op(Name("+", 0), es) => sums = Op(Name("+", 0), es) :: sums
      case Op(Name("*", 0), es) => pros = Op(Name("*", 0), es) :: pros
      case Op(Name("/", 0), es) => if (division)
        divis = Op(Name("/", 0), es) :: divis;
      else
        vars = x :: vars
      case _ => vars = x :: vars
    })
    (nums, vars, sums, pros, divis)
  }
  // 2*x => (2,x)
  def tearProduct(e: Expr): (Double, Option[Expr]) = e match {
    case Lit(GDouble(n)) => (n, None)
    case Op(Name("*", 0), es) =>
      val ses = sortExprs(es, true)
      val num = aggregateNums("+", Nil, ses._1) match {
        case Lit(GDouble(n)) => n
      }
      val rest = es.diff(ses._1) match {
        case Nil => None
        case t :: Nil => Some(t)
        case l => Some(Op(Name("*", 0), l))
      }
      (num, rest)
    case _ => (1.0, Some(e))
  }

  def writeToFile(fname: String, txt: String): Unit = {
    var append = true
    var out_file = new java.io.FileOutputStream(fname, append)
    var out_stream = new java.io.PrintStream(out_file, append)
    out_stream.print(txt)
    out_stream.close

  }

  def unFlattenOp(e: Expr): Expr = e match {
    case Lit(_) => e
    case Var(_) => e
    case Op(f, List(n)) => Op(f, List((unFlattenOp(n))))
    case Op(f, es) =>
      val unes = es.map(unFlattenOp(_))
      unes.drop(1).foldLeft(unes(0))((r, x) =>
        Op(f, r :: x :: Nil))

    case _ => e
  }
   def simplifyEE(exp: Expr): Expr = {
    exp match {
      case Op(Name(f, 0), x :: y :: Nil) => f match {
        case "+" => (x, y) match {
          case (Lit(GInt(n1)), Lit(GInt(n2)))       => Lit(GDouble(n1 + n2))
          case (Lit(GInt(n1)), Lit(GDouble(n2)))    => Lit(GDouble(n1 + n2))
          case (Lit(GDouble(n1)), Lit(GInt(n2)))    => Lit(GDouble(n1 + n2))
          case (Lit(GDouble(n1)), Lit(GDouble(n2))) => Lit(GDouble(n1 + n2))
          case (Lit(GInt(0)), n2)                   => n2
          case (n1, Lit(GDouble(0)))                => n1
          case (Lit(GDouble(0)), n2)                => n2
          case (n1, Lit(GInt(0)))                   => n1

          case _                                    => Op(Name(f, 0), x :: y :: Nil)
        }
        case "-" => (x, y) match {
          case (Lit(GInt(n1)), Lit(GInt(n2)))       => Lit(GDouble(n1 - n2))
          case (Lit(GInt(n1)), Lit(GDouble(n2)))    => Lit(GDouble(n1 - n2))
          case (Lit(GDouble(n1)), Lit(GInt(n2)))    => Lit(GDouble(n1 - n2))
          case (Lit(GDouble(n1)), Lit(GDouble(n2))) => Lit(GDouble(n1 - n2))
          case (Lit(GInt(0)), n2)                   => Op(Name("*", 0), Lit(GDouble(-1)) :: n2 :: Nil)
          case (n1, Lit(GInt(0)))                   => n1
          case _                                    => Op(Name(f, 0), x :: y :: Nil)
        }
        case "*" =>
          (x, y) match {
            case (Lit(GInt(n1)), Lit(GInt(n2))) => Lit(GDouble(n1 * n2))
            case (Lit(GInt(n1)), Lit(GDouble(n2))) => Lit(GDouble(n1 * n2))
            case (Lit(GDouble(n1)), Lit(GInt(n2))) => Lit(GDouble(n1 * n2))
            case (Lit(GDouble(n1)), Lit(GDouble(n2))) => Lit(GDouble(n1 * n2))
            case (Lit(GInt(0)), n2) => Lit(GDouble(0))
            case (Lit(GDouble(0)), n2) => Lit(GDouble(0))
            case (n1, Lit(GInt(0))) => Lit(GDouble(0))
            case (n1, Lit(GDouble(0))) => Lit(GDouble(0))
            case (Lit(GInt(n)), Op(Name("*", 0), Lit(GInt(n1)) :: y :: Nil)) => mkOp("*", Lit(GDouble(n * n1)), y)
            case (Lit(GDouble(n)), Op(Name("*", 0), Lit(GDouble(n1)) :: y :: Nil)) => mkOp("*", Lit(GDouble(n * n1)), y)
            case _ => Op(Name(f, 0), x :: y :: Nil)
          }
        case "/" => (x, y) match {
          case (Lit(GInt(n1)), Lit(GInt(n2)))       => Lit(GDouble(n1.toDouble / n2.toDouble))
          case (Lit(GInt(n1)), Lit(GDouble(n2)))    => Lit(GDouble(n1.toDouble / n2.toDouble))
          case (Lit(GDouble(n1)), Lit(GInt(n2)))    => Lit(GDouble(n1.toDouble / n2.toDouble))
          case (Lit(GDouble(n1)), Lit(GDouble(n2))) => Lit(GDouble(n1.toDouble / n2.toDouble))
          case (Lit(GInt(0)), n2)                   => Lit(GDouble(0))
          case (n1, Lit(GInt(0)))                   => error("Divided by zero")
          case (n1, Lit(GDouble(0)))                => error("Divided by zero")
          case _ =>
            if (x == y)
              Lit(GInt(1))
            else
              Op(Name(f, 0), x :: y :: Nil)
        }
        case _ => Op(Name(f, 0), x :: y :: Nil)

      }
      case Op(Name("+", 0), args) => Op(name("+"), args map simplify)
      case Op(Name("-", 0), args) => Op(name("-"), args map simplify)
      case Op(Name("*", 0), args) => Op(name("*"), args map simplify)
      case Op(Name("/", 0), args) => Op(name("/"), args map simplify)
      case _                      => exp
    }

  }
}

